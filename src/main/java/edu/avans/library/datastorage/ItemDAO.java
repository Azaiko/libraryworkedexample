/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.avans.library.datastorage;

import edu.avans.library.domain.Item;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Rodi
 */
public class ItemDAO {
    
    public ItemDAO() { 
        
    }
    
    public Item doFindItem(int itemID){
        Item item = null;
        
 // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT * FROM item WHERE ItemID = " + itemID + ";");

            if (resultset != null) {
                try {
                    // The membershipnumber for a member is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    if (resultset.next()) {
                        int itemIdFromDb = resultset.getInt("ItemID");
                        String typeFromDb = resultset.getString("Type");
                        String titleFromDb = resultset.getString("Title");
                        String creatorFromDb = resultset.getString("Creator");
                        int copiesFromDb = resultset.getInt("Copies");

                        item = new Item(
                                itemIdFromDb,
                                typeFromDb,
                                titleFromDb,
                                creatorFromDb,
                                copiesFromDb);

                        /*member.setStreet(resultset.getString("Street"));
                        member.setHouseNumber(resultset.getString("HouseNumber"));
                        member.setCity(resultset.getString("City"));
                        member.setFine(resultset.getDouble("Fine"));*/
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    item = null;
                }
            }
            // else an error occurred leave 'member' to null.

            // We had a database connection opened. Since we're finished,
            // we need to close it.
            connection.closeConnection();
        }

        return item;        
    }
    
}
