/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.avans.library.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rodi
 */
public class CD {
    private int ID;
    private String title;
    private String artist;
    private int year;
    
    private List<Copy> copies;
    
    public CD(int ID, String title, String artist, int year) {
        this.ID = ID;
        this.title = title;
        this.artist = artist;
        this.year = year;
        
        copies = new ArrayList<>();
    }
}
