/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.avans.library.domain;

/**
 *
 * @author Rodi
 */
public class Item {
    
    private int itemId;
    private String itemType;
    private String title;
    private String creator;
    private int copies;
    
    public Item(int itemId, String itemType, String title, String creator, int copies){
        this.itemId = itemId;
        this.itemType = itemType;
        this.title = title;
        this.creator = creator;
        this.copies = copies;
    }
    
    public int getCopies(){
        return copies;
    }
    
    public int setCopies(int copy){
        copies = copies + copy;
        return copies;
    }
    
    public void setItemId(int itemId){
        this.itemId = itemId;
    }
    
    public int getItemId(){
        return itemId;
    }
    
    public void setItemType(String itemType){
        this.itemType = itemType;
    }
    
    public String getItemType(){
        return itemType;
    }
    
    public void setTitle(String title){
        this.title = title;
    }
    
    public String getTitle(){
        return title;
    }
    
    public void setCreator(String creator) {
        this.creator = creator;
    }
    
    public String getCreator(){
        return creator;
    }
}
