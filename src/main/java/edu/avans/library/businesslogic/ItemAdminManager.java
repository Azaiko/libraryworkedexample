/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.avans.library.businesslogic;
import edu.avans.library.datastorage.ItemDAO;
import edu.avans.library.domain.Item;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Rodi
 */
public class ItemAdminManager {
    private Map<Integer, Item> items;
    
    public ItemAdminManager(){
        items = new HashMap<>();
    }
    
    public Item findItem(int itemId){
        Item item = items.get(itemId);        
        
        if (item == null) {
            ItemDAO itemDAO = new ItemDAO();
            item = itemDAO.doFindItem(itemId);
            
            if (item != null){
                items.put(itemId, item);
            }
        }
        
        return item;
    }
}
